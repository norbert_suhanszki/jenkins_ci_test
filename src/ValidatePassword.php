<?php

class ValidatePassword
{
    const MIN_LENGTH = 6;
    const MAX_LENGTH = 10;

    public function validLength($password){
        $password_length = strlen($password);
        return $password_length >= self::MIN_LENGTH && $password_length <= self::MAX_LENGTH;
    }
}